<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function create() {
        return view('auth.login');
    }

    public function login(Request $request){
        
        $infoLogin = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        if (Auth::attempt($infoLogin)){
            $request->session()->regenerate();
            if(Auth::user()->is_active === true ){
                return to_route('dashboard');
            }
            else{
                Auth::logout();
                return redirect('/login')->withErrors(['error'=>'Akun anda sudah di nonaktifkan!']);
            }
            }

        else{
            return redirect('/login')->withErrors(['error' => 'Password atau Email anda salah!']);
        }
    }

    public function logout(){

        Auth::logout();
        return redirect('/login');
        
    }
}
