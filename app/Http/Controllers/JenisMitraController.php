<?php

namespace App\Http\Controllers;

use App\Models\JenisMitra;
use App\Models\Mitra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;

class JenisMitraController extends Controller
{
    public function index()
    {
        $jenisMitra = DB::table('jenis_mitra')->get()->map(function($jenisMitra) {
            $mitra = Mitra::withCount('dataKerjasama as jumlahKerjasama')->where('jenis_mitra', $jenisMitra->nama)->get();
            return [
                'id' => $jenisMitra->id,
                'nama' => $jenisMitra->nama,
                'jumlahKerjasama' => $mitra->sum('jumlahKerjasama'),
                'jumlahMitra' => $mitra->count(),
            ];
        });
        return view('referensi.admin-jenis-mitra', compact('jenisMitra'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:50'
        ]);

        JenisMitra::create([
            'nama' => $request->nama
        ]);

        return to_route('jenis-mitra.index')->with('success', 'Jenis Mitra berhasil di tambahkan');
    }

    public function update(Request $request, JenisMitra $jenisMitra)
    {
        $request->validate([
            'nama' => 'required|string|max:50'
        ]);

        $mitraCount = Mitra::where('jenis_mitra', $jenisMitra->nama)->count();
        if($mitraCount > 0){
            return to_route('jenis-mitra.index')->with('error', 'Jenis Mitra tidak bisa diedit karena memiliki mitra');
        }

        $jenisMitra->update([
            'nama' => $request->nama
        ]);

        return to_route('jenis-mitra.index')->with('success', 'Jenis mitra berhasil di edit');
    }

    public function destroy(JenisMitra $jenisMitra)
    {
        $mitraCount = Mitra::where('jenis_mitra', $jenisMitra->nama)->count();
        if($mitraCount > 0){
            return to_route('jenis-mitra.index')->with('error', 'Jenis Mitra tidak bisa dihapus karena memiliki mitra');
        }

        $jenisMitra->delete();

        return to_route('jenis-mitra.index')->with('success', 'Jenis Mitra berhasil di hapus');
    }

    public function export()
    {
        $jenisMitra = DB::table('jenis_mitra')->get()->map(function($jenisMitra, $i) {
            $mitra = Mitra::withCount('dataKerjasama as jumlahKerjasama')->where('jenis_mitra', $jenisMitra->nama)->get();
            return [
                'No' => $i + 1,
                'Jenis Mitra' => $jenisMitra->nama,
                'Jumlah Mitra' => $mitra->sum('jumlahKerjasama'),
                'Jumlah Kerjasama' => $mitra->count(),
            ];
        });
        return (new FastExcel($jenisMitra))->download('Jenis Mitra.xlsx');
    }
}
