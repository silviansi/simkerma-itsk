<?php

namespace App\Http\Controllers;

use App\Models\DataKerjasama;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke()
    {
        $hariIni = Carbon::now();
        $tujuhHariKedepan = $hariIni->copy()->addDays(7);
        
        if(Auth::user()->role === 'Admin' || Auth::user()->role === 'User'){
            // dd('diadmin');
            $semuaKerjasama = DataKerjasama::where('data_kerjasama.user_id', '=', Auth::user()->id)->count();
            $kerjasamaAktif = DataKerjasama::whereDate('tanggal_akhir', '>', now())
                            ->where('data_kerjasama.user_id', '=', Auth::user()->id)
                            ->count();
            $kerjasamaBerakhir = DataKerjasama::whereDate('tanggal_akhir', '<', now())
                            ->where('data_kerjasama.user_id', '=', Auth::user()->id)
                            ->count();
            $kerjasamaAkanBerakhir = DataKerjasama::orderBy('created_at', 'desc')
                                ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                ->where('tanggal_akhir','>', $hariIni)
                                ->where('tanggal_akhir','<',$tujuhHariKedepan)
                                ->where('data_kerjasama.user_id', '=', Auth::user()->id)
                                ->count();
            $tanggalMouCounts = DataKerjasama::select(DataKerjasama::raw('YEAR(tanggal_mou) as year'), DataKerjasama::raw('count(*) as jumlah_tanggal_mou'))
                                ->groupBy(DataKerjasama::raw('YEAR(tanggal_mou)'))
                                ->where('data_kerjasama.user_id', '=', Auth::user()->id)
                                ->get();
            $tanggalAkhirCounts = DataKerjasama::select(DataKerjasama::raw('YEAR(tanggal_akhir) as year'), DataKerjasama::raw('count(*) as jumlah_tanggal_akhir'))
                                ->groupBy(DataKerjasama::raw('YEAR(tanggal_akhir)'))
                                ->where('data_kerjasama.user_id', '=', Auth::user()->id)
                                ->get();
        }
        else{
            $semuaKerjasama = DataKerjasama::count();
            $kerjasamaAktif = DataKerjasama::whereDate('tanggal_akhir', '>', now())->count();
            $kerjasamaBerakhir = DataKerjasama::whereDate('tanggal_akhir', '<', now())->count();
            $kerjasamaAkanBerakhir = DataKerjasama::orderBy('created_at', 'desc')
                                ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                ->where('tanggal_akhir','>', $hariIni)
                                ->where('tanggal_akhir','<',$tujuhHariKedepan)
                                ->count();
            $tanggalMouCounts = DataKerjasama::select(DataKerjasama::raw('YEAR(tanggal_mou) as year'), DataKerjasama::raw('count(*) as jumlah_tanggal_mou'))
                                ->groupBy(DataKerjasama::raw('YEAR(tanggal_mou)'))
                                ->get();
            $tanggalAkhirCounts = DataKerjasama::select(DataKerjasama::raw('YEAR(tanggal_akhir) as year'), DataKerjasama::raw('count(*) as jumlah_tanggal_akhir'))
                                ->groupBy(DataKerjasama::raw('YEAR(tanggal_akhir)'))
                                ->get();
        }

        $mouCountsArray = $tanggalMouCounts->keyBy('year')->toArray();
        $akhirCountsArray = $tanggalAkhirCounts->keyBy('year')->toArray();


        $combinedCounts = [];

        foreach ($mouCountsArray as $year => $data) {
            $combinedCounts[] = [
                'year' => $data['year'],
                'jumlah_tanggal_mou' => $data['jumlah_tanggal_mou'],
                'jumlah_tanggal_akhir' => $akhirCountsArray[$year]['jumlah_tanggal_akhir'] ?? 0
            ];
        }
        
        // Menambahkan data jumlah_tanggal_akhir yang tidak ada di $mouCountsArray
        foreach ($akhirCountsArray as $year => $data) {
            if (!array_key_exists($year, $mouCountsArray)) {
                $combinedCounts[] = [
                    'year' => $data['year'],
                    'jumlah_tanggal_mou' => 0,
                    'jumlah_tanggal_akhir' => $data['jumlah_tanggal_akhir']
                ];
            }
        }

        usort($combinedCounts, function($a, $b) {
            return $a['year'] <=> $b['year'];
        });



        return view ('dashboard/admin-dashboard', compact('semuaKerjasama', 'kerjasamaAktif', 'kerjasamaBerakhir', 'kerjasamaAkanBerakhir', 'combinedCounts' ));
    }
}