<?php

namespace App\Http\Controllers;

use App\Http\Requests\TemplateRequest;
use App\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TemplateController extends Controller
{
    public function index(Request $request)
    {
        $template = Template::get();
        return view('template.template', compact('template'));
    }

    public function store(TemplateRequest $request)
    {
        $request->user()->template()->create([
            ...$request->validated(),
            'file' => $request->file('file')->store('Data-Template')
        ]);

        return to_route('template.index')->with('success', 'Data Template Berhasil di tambahkan');
    }

    public function update(TemplateRequest $request, Template $template)
    {
        if($request->hasFile('file')){
            Storage::delete($template->file);
        }

        $template->update([
            ...$request->validated(),
            'file' => $request->hasFile('file') 
                        ? $request->file('file')->store('Data-Template')
                        : $template->file
        ]);

        return to_route('template.index')->with('success', 'Data Template Berhasil di edit');
    }

    public function toggleStatus(Template $template)
    {
        $template->is_active = !$template->is_active;
        $template->save();

        return response()->json(['success' => true]);
    }

    public function downloadTemplate(Template $template)
    {
        $extension = pathinfo(Storage::url($template->file), PATHINFO_EXTENSION);
        return Storage::download($template->file, $template->nama . '.' . $extension);
    }
}
