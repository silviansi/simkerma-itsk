@extends('base')
@section('title', 'SIMKERMA | Template MoU')
@section('konten')
    <div class="row align-items-center mb-0">
        <div class="col-md-8">
            <h6 class="page-title">Template MoU</h6>
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="#">SIMKERMA</a></li>
                <li class="breadcrumb-item active" aria-current="page">Template MoU</a></li>
            </ol>
        </div>
    </div>
    <div class="row align-items-center mb-0">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="float-end d-md-block">
                <div class="my-3 text-center">
                    <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal"
                        data-bs-target="#modelTambah">+ Tambahkan Template MoU</button>
                </div>
                <!-- sample modal content -->
                <div id="modelTambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modelTambahLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <form action="{{ route('template.store') }}" method="POST" class="modal-content"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title" id="modelTambahLabel">Tambah Template MoU
                                </h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <div class="mb-3">
                                        <label for="namaMoU" class="form-label">Nama MoU</label>
                                        <input type="text" class="form-control" id="namaMoU" name="nama"
                                            placeholder="Masukkan Nama MoU">
                                    </div>
                                    <div class="mb-3">
                                        <div class="mb-3">
                                            <label for="jenisMoU" class="form-label">Status Template</label>
                                            <select class="form-control select2" id="jenisMoU" name="is_active">
                                                <option>Pilih Status Template</option>
                                                <option value="1">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                            </select>
                                        </div>

                                        <div class="col-md-12 pb-3">
                                            <label for="judul-mou" class="form-label">Unggah Template MoU</label>
                                            <div class="col-sm-6">
                                                <div class="file-upload-wrapper" data-text="Pilih Template MoU">
                                                    <input name="file" type="file" class="file-upload-field"
                                                        value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect"
                                    data-bs-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary waves-effect waves-light"><i
                                        class="fas fa-save"></i> Simpan</button>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>
        </div>
    </div>
    <!-- end modal tambah -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 2%;">Nomor</th>
                                    <th style="width: 25%;">Nama MoU</th>
                                    <th class="text-center" style="width: 5%;">Status Template</th>
                                    <th class="text-center" style="width: 5%;">Unduh Template</th>
                                    <th class="text-center" style="width: 5%;">Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($template as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->nama }}</td>
                                        <td class="text-center">
                                            <div class="tmbl-tggl">
                                                <input type="checkbox" name=""
                                                    id="toggle-status{{ $loop->iteration }}"
                                                    class="tggl-btn toggle-template" data-template-id="{{ $data->id }}"
                                                    {{ $data->is_active ? 'checked' : '' }}>
                                                <label for="toggle-status{{ $loop->iteration }}" class="onbtn"><i
                                                        class="fa-solid fa-check"></i></label>
                                                <label for="toggle-status{{ $loop->iteration }}" class="offbtn"><i
                                                        class="fa-solid fa-xmark"></i></label>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <!-- Button untuk unduh -->
                                            <a href="{{ route('template.download', $data->id) }}" class="btn btn-info"
                                                style="padding: 6px 12px;">
                                                <i class=" dripicons-download " style="color: #E9ECEF"></i>
                                            </a>

                                        </td>
                                        <td class="text-center">
                                            <!-- Button untuk mengedit -->
                                            <button type="button" class="btn btn-warning" style=" padding: 6px 12px;"
                                                data-bs-toggle="modal" data-bs-target="#modelEdit{{ $data->id }}"><i
                                                    class="fas fa-pencil-alt" style="color: #E9ECEF"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach ($template as $data)
        <!-- Modal untuk mengedit -->
        <div id="modelEdit{{ $data->id }}" class="modal fade" tabindex="" role="dialog"
            aria-labelledby="modelEdit{{ $data->id }}Label" aria-hidden="true">
            <div class="modal-dialog">
                <form action="{{ route('template.update', $data->id) }}" method="POST" class="modal-content"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel">Edit Data Template MoU
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <!-- Form edit data disini -->
                    <div class="modal-body">
                        <div>
                            <div class="mb-3">
                                <label for="namaMoU" class="form-label">Nama MoU</label>
                                <input type="text" class="form-control" required id="namaMoU"
                                    placeholder="Masukkan Nama MoU" name="nama" value="{{ $data->nama }}">
                            </div>
                            <div class="mb-3">
                                <div class="mb-3">
                                    <label for="jenisMoU" class="form-label">Status
                                        Template</label>
                                    <select class="form-control select2" id="jenisMoU" name="is_active" required>
                                        <option>Pilih Status Template</option>
                                        <option value="1" {{ $data->is_active ? 'selected' : '' }}>
                                            Aktif</option>
                                        <option value="0" {{ $data->is_active ? '' : 'selected' }}>
                                            Tidak Aktif</option>
                                    </select>
                                </div>
                                <div class="col-md-12 pb-3">
                                    <label for="judul-mou" class="form-label">Unggah Template MoU</label>
                                    <div class="col-sm-6">
                                        <div class="file-upload-wrapper" data-text="Pilih template MoU">
                                            <input name="file" type="file" class="file-upload-field"
                                                value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light"><i
                                class="fas fa-save"></i>
                            Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    @endforeach
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $(".toggle-template").click(function(e) {
                e.preventDefault();
                var templateId = $(this).data('template-id');
                var toggle = $(this);
                Swal.fire({
                    title: "Anda yakin?",
                    text: "Aksi ini tidak dapat dibatalkan!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Ya, ubah!",
                    cancelButtonText: "Batal"
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "POST",
                            url: "{{ route('template.toggle-status', '') }}/" + templateId,
                            data: {
                                _token: "{{ csrf_token() }}",
                            },
                            success: function(response) {
                                Swal.fire({
                                    title: "Sukses!",
                                    text: "Status berhasil diubah.",
                                    icon: "success"
                                }).then((result) => {
                                    toggle.prop('checked', !toggle.prop(
                                        'checked'));
                                });
                            },
                            error: function(error) {
                                console.log(error)
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Terjadi Kesalahan!',
                                });
                            }
                        });
                    }
                });
            });
        });
    </script>
@endpush
