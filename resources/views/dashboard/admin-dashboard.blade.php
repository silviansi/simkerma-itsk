@extends('base')
@section('title', 'SIMKERMA | Dashboard')
@section('konten')
    <div class="row">
        <h6 class="page-title">Dashboard</h6>
        <h4 class="page-title">Selamat Datang di SIMKERMA</h4>
        <div class="col-xl-3 col-md-6 ">
            <div class="card mini-stat bg-danger text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-start mini-stat-img me-4 pt-2">
                            <h1>{{ $semuaKerjasama }}</h1>
                        </div>
                        <h5 class="font-size-16 text-uppercase text-white-50">SEMUA KERJA SAMA</h5>
                    </div>
                    <div class="pt-2">
                        <div class="float-end">
                            <a href="/kerjasama" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                        </div>

                        <p class="text-white-50 mb-0 mt-1">Lihat Selengkapnya</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-start mini-stat-img me-4 pt-2">
                            <h1>{{ $kerjasamaAktif }}</h1>
                        </div>
                        <h5 class="font-size-16 text-uppercase text-white-50">KERJA SAMA AKTIF</h5>
                    </div>
                    <div class="pt-2">
                        <div class="float-end">
<<<<<<< HEAD
                            <a href="/kerjasama-Aktif" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
=======
                            <a href="kerjasama-Aktif" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
>>>>>>> 553f5195489713dbde4cc304d3492d235652dfa3
                        </div>

                        <p class="text-white-50 mb-0 mt-1">Lihat Selengkapnya</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-success text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-start mini-stat-img me-4 pt-2">
                            <h1>{{ $kerjasamaBerakhir }}</h1>
                        </div>
                        <h5 class="font-size-16 text-uppercase text-white-50">kERJA SAMA BERAKHIR</h5>
                    </div>
                    <div class="pt-2">
                        <div class="float-end">
                            <a href="/kerjasama-Berakhir" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                        </div>

                        <p class="text-white-50 mb-0 mt-1">Lihat Selengkapnya</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-warning text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-start mini-stat-img me-4 pt-2">
                            <h1>{{ $kerjasamaAkanBerakhir }}</h1>
                        </div>
                        <h5 class="font-size-16 text-uppercase text-white-50">KERJA SAMA AKAN BERKAHIR</h5>
                    </div>
                    <div class="pt-2">
                        <div class="float-end">
<<<<<<< HEAD
                            <a href="/kerjasama-AkanBerakhir" class="text-white-50"><i
                                    class="mdi mdi-arrow-right h5"></i></a>
=======
                            <a href="/kerjasama-AkanBerakhir" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
>>>>>>> 553f5195489713dbde4cc304d3492d235652dfa3
                        </div>

                        <p class="text-white-50 mb-0 mt-1">Lihat Selengkapnya</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-xl-9">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title mb-4">Statistik Kerja Sama</h4>

                    <div class="row justify-content-center">
                        <div class="col-sm-4">
                            <div class="text-center">
                                <div style="width: 20px; height: 20px; background-color:#3C4CCF" class="m-auto"></div>
                                <p class="text-muted">Masuk</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="text-center">
                                <div style="width: 20px; height: 20px; background-color:#EE4E4E" class="m-auto"></div>
                                <p class="text-muted">Berakhir</p>
                            </div>
                        </div>
                    </div>

                    <div id="chart"></div>

                </div>
            </div>
        </div> <!-- end col -->
        <div class="col-xl-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Jumlah Kerjasama</h4>

                    <div id="ct-donut" class="ct-chart wid"></div>

                    <div class="mt-4">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td><span class="badge bg-primary">A</span></td>
                                    <td>Kerjasama Aktif</td>
                                    <td class="text-end">@json($kerjasamaAktif)</td>
                                </tr>
                                <tr>
                                    <td><span class="badge bg-success">B</span></td>
                                    <td>Kerjasama Berakhir</td>
                                    <td class="text-end">@json($kerjasamaBerakhir)</td>
                                </tr>
                                <tr>
                                    <td><span class="badge bg-warning">C</span></td>
                                    <td>Kerjasama Akan Berakhir</td>
                                    <td class="text-end">@json($kerjasamaAkanBerakhir)</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- end row -->
@endsection

@push('script')
    <!-- Morris Chart JS -->
    {{-- <script src="assets/libs/morris.js/morris.min.js"></script>
    <script src="assets/libs/raphael/raphael.min.js"></script>
    <script src="assets/js/custom.js"></script> --}}
    <script>
        // Morris Chart
        Morris.Bar({
          element: 'chart',
          data: @json($combinedCounts),
          xkey: 'year',
          ykeys: ['jumlah_tanggal_mou', 'jumlah_tanggal_akhir'],
          labels: ['masuk', 'berakhir'],
          hideHover: 'auto',
          barColors: ['#3C4CCF', '#EE4E4E'],
          stacked: false,
          yLabelFormat: function (y) { return Math.round(y); }
        });
    </script>

    <!-- Plugin Js/Chartist JS-->
    <script src="assets/libs/chartist/chartist.min.js"></script>
    <script src="assets/libs/chartist-plugin-tooltips/chartist-plugin-tooltip.min.js"></script>

    <script>
        // Membuat chart donat dengan Chartist.js
        var chart = new Chartist.Pie("#ct-donut", {
            series: [@json($kerjasamaAktif), @json($kerjasamaBerakhir), @json($kerjasamaAkanBerakhir)],
            labels: [1, 2, 3]
        }, {
            donut: true,
            showLabel: false,
            plugins: [Chartist.plugins.tooltip()]
        });

        $(".peity-donut").each(function() {
            $(this).peity("donut", $(this).data());
        });

        $(".peity-line").each(function() {
            $(this).peity("line", $(this).data());
        });
    </script>
@endpush
