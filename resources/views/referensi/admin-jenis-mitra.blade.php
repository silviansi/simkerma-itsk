@extends('base')
@section('title', 'SIMKERMA | Jenis Mitra')
@section('konten')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h6 class="page-title">Data Jenis Mitra</h6>
                {{-- Breadcrumb --}}
            </div>
            <div class="col-md-4">
                <div class="d-flex flex-column flex-md-row justify-content-end align-items-md-center gap-3">
                    <a href="{{ route('jenis-mitra.export') }}" class="btn btn-primary waves-effect waves-light"><i
                            class="mdi mdi-content-save"></i> Ekspor</a>
                    <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal"
                            data-bs-target="#modalTambahJenisMitra"><i class="mdi mdi-plus"></i>
                        Tambah Jenis Mitra Baru
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="datatable" class="table table-bordered dt-responsive nowrap"
                               style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Jenis Mitra</th>
                                <th>Jumlah Mitra</th>
                                <th>Jumlah Kerja Sama</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($jenisMitra as $item)
                                <tr>
                                    <td>{{ $item['nama'] }}</td>
                                    <td>{{ $item['jumlahMitra'] }}</td>
                                    <td>{{ $item['jumlahKerjasama'] }}</td>
                                    <td>
                                        <div class="row gap">
                                            <div class="col">
                                                {{-- Edit Button --}}
                                                <button type="button" class="btn btn-warning waves-effect"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#modalEditJenisMitra{{ $item['id'] }}"><i
                                                        class="mdi mdi-pencil" style="color: black"></i></button>
                                                {{-- Delete Button --}}
                                                <form action="{{ route('jenis-mitra.destroy', $item['id']) }}"
                                                      method="POST" class="d-inline delete-jenis-mitra">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger waves-effect deleteBtn">
                                                        <i
                                                            class="mdi mdi-trash-can" style="color: black"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- MODAL TAMBAH JENIS MITRA --}}
        <div id="modalTambahJenisMitra" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form action="{{ route('jenis-mitra.store') }}" method="POST">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabel2">Tambah Jenis Mitra Baru</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="col">
                                <div class="col-md-12 pb-3">
                                    <label for="nama" class="form-label">Nama Jenis Mitra</label>
                                    <input type="text" class="form-control" id="nama" name="nama" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        {{-- MODAL EDIT JENIS MITRA --}}
        @foreach ($jenisMitra as $item)
            <div id="modalEditJenisMitra{{ $item['id'] }}" class="modal fade bs-example-modal-center" tabindex="-1"
                 role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <form action="{{ route('jenis-mitra.update', $item['id']) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel2">Edit Jenis Mitra</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="col">
                                    <div class="col-md-12 pb-3">
                                        <label for="nama" class="form-label">Nama Jenis Mitra</label>
                                        <input type="text" class="form-control" id="nama" value="{{ $item['nama'] }}"
                                               name="nama" required>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        @endforeach


        @endsection

        @push('script')
            <script>
                const allJenisMitra = document.querySelectorAll('.delete-jenis-mitra');
                allJenisMitra.forEach(function (jenisMitra) {
                    jenisMitra.addEventListener('submit', function (e) {
                        e.preventDefault();
                        Swal.fire({
                            title: 'Apakah Anda yakin?',
                            text: "Anda tidak akan dapat mengembalikan ini!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#d33',
                            cancelButtonColor: '#3085d6',
                            confirmButtonText: 'Ya, Hapus Data!',
                            cancelButtonText: 'Batal'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                jenisMitra.submit();
                            }
                        });
                    })
                });
            </script>
    @endpush
