<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIMKERMA</title>
    <!-- Icon Title -->
    <link rel="shortcut icon" href="assets/images/simkerma-logo-small.png">
    <!-- Bootstrap Css -->
    <link href="assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/landing.css">
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container p-5">
        <h2 class="form-title text-center pb-5">Formulir Pengajuan Kerja Sama</h2>
        <form action="/send-datakerjasama" method="POST" class="form" id="form"  enctype="multipart/form-data">
            @csrf
            <div class="row mb-4 justify-content-center">
                <label for="instansi" class="col-sm-2 col-form-label">Nama Instansi</label>
                <div class="col-sm-6 select-container">
                    <select class="form-select select-element nama_mitra" id="instansi" name="nama_mitra" required>
                        <option value="" selected disabled hidden>Masukkan nama instansi</option>
                        @foreach ($mitra as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_mitra }}</option>    
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="nama" class="col-sm-2 col-form-label">Nama Lengkap</label>
                <div class="col-sm-6">
                    <input class="form-control" name="penanggung_jawab" type="text" id="nama" placeholder="Masukkan nama lengkap" required>
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="email" class="col-sm-2 col-form-label">Alamat Email</label>
                <div class="col-sm-6">
                    <input class="form-control" name="email" type="email" id="email" placeholder="Masukkan alamat email" required>
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="no_telp" class="col-sm-2 col-form-label">Nomor Handphone</label>
                <div class="col-sm-6">
                    <input class="form-control" name="no_telp" type="text" id="no_telp" placeholder="08xxxxxxxxxx" required>
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="kegiatan" class="col-sm-2 col-form-label">Nama Kegiatan</label>
                <div class="col-sm-6">
                    <input class="form-control" name="deskripsi" type="text" id="kegiatan" placeholder="Masukkan nama kegiatan" required>
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="nomor_surat_mitra" class="col-sm-2 col-form-label">No. Surat Mitra</label>
                <div class="col-sm-6">
                    <input class="form-control" name="no_surat_mitra" type="text" id="nomor_surat_mitra" placeholder="Masukkan nomor surat mitra " required>
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="tgl-mulai" class="col-sm-2 col-form-label">Tanggal Mulai</label>
                <div class="col-sm-6">
                    <input class="form-control" name="tanggal_mou" type="date" id="tgl-mulai" placeholder="Pilih tanggal mulai" required>
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="tgl-akhir" class="col-sm-2 col-form-label">Tanggal Berakhir</label>
                <div class="col-sm-6">
                    <input class="form-control" name="tanggal_akhir" type="date" id="tgl-akhir" placeholder="Pilih tanggal berakhir" required>
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="doc-mou" class="col-sm-2 col-form-label">Dokumen MoU</label>
                <div class="col-sm-6">
                    <div class="file-upload-wrapper" data-text="Pilih dokumen MoU">
                        <input name="file" id="doc-mou" type="file" class="file-upload-field" value="" accept=".pdf" required>
                    </div>
                </div>
            </div>
            <div class="button-group d-flex justify-content-end gap-2 pt-5">
                <button type="button" class="btn btn-batal fw-bold" onclick="window.location.href='/'">Batal</button>
                <button type="submit"  class="btn btn-kirim fw-bold">Kirim</button>
            </div>
        </form>

    </div>


    <script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    {{-- import sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
    $(window).on("beforeunload", function () {
        $("form")[0].reset();
    });
    $("form").on("change", ".file-upload-field", function () {
    var fileName = $(this).val();
    var maxLength = 20; 
    var truncatedName = fileName.substring(0, maxLength);

    if (fileName.length > maxLength) {
        var extension = fileName.split('.').pop(); 
        truncatedName += '.' + extension; 

        $(this)
        .parent(".file-upload-wrapper")
        .attr("data-text", truncatedName);
    } else {
        $(this)
        .parent(".file-upload-wrapper")
        .attr("data-text", fileName);
    }
    });
    $(".nama_mitra").on("change", function(){
        var selectedValue = parseInt(document.getElementById('instansi').value); 
        var nama = document.getElementById('nama');
        var email = document.getElementById('email');
        var nomor_hp = document.getElementById('no_telp');

        $.ajax({
                type: "GET",
                url: "{{ route('get.mitra') }}",
                success : function(response){
                    var isTrue = false;
                    var name_db = '';
                    var email_db = '';
                    var nomor_hp_db = '';
                    for(let i = 0; i < response.length; i++ ){
                        if(response[i].id === selectedValue){
                            isTrue = true;
                            name_db = response[i].penanggung_jawab;
                            email_db = response[i].email;     
                            nomor_hp_db = response[i].no_telp;
                        }
                    }
                    if(isTrue){
                        nama.value = name_db;
                        email.value = email_db;
                        nomor_hp.value = nomor_hp_db;
                        nama.readOnly = true;
                        email.readOnly = true;
                        nomor_hp.readOnly = true;
                        nama.style.backgroundColor = "#ccc";
                        email.style.backgroundColor = "#ccc";
                        nomor_hp.style.backgroundColor = "#ccc";
                    }
                    else{
                        nama.value = '';
                        email.value = '';
                        nomor_hp.value = '';
                        nama.readOnly = false;
                        email.readOnly = false;
                        nomor_hp.readOnly = false;
                        nama.style.backgroundColor = "#fff";
                        email.style.backgroundColor = "#fff";
                        nomor_hp.style.backgroundColor = "#fff";
                    }
                }
            })
    })


    $(document).ready(function(){
        $('form').on('submit', function(event){
            event.preventDefault();
            var formData = new FormData(this);

            $.ajax({
                url:    "{{ route('store-kerjasama') }}",
                method: "POST",
                data:   formData,
                contentType : false,
                processData : false,
                success: function(response){
                    if (response && response.error){
                        Swal.fire({
                            title: "Error!",
                            text: response.error,
                            icon: "error"
                        })    
                    }
                    else{
                        $("#instansi").val("").trigger("change");
                        $(".file-upload-field").val(""); 
                        $(".file-upload-wrapper").attr("data-text", "Pilih dokumen MoU");
                        document.getElementById('form').reset();
                        Swal.fire({
                            html: "untuk Informasi Lebih Lanjut Hubungi <br/> 0811-3229-9222",
                            title: "Sukses!",
                            icon: "success"
                        })
                    }
                },
                error: function(response){
                    Swal.fire({
                    title: "Error!",
                    text: "Terjadi Kesalahan Pada Server",
                    icon: "error"
                    })
                }
            })
        })
    })
    $(document).ready(function() {
        $("#instansi").select2({
            tags: true,
            allowClear: true,
            placeholder: "Masukkan nama instansi"
        });
    });
    </script>
</body>
</html>