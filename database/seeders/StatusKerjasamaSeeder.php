<?php

namespace Database\Seeders;

use App\Models\StatusKerjasama;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StatusKerjasamaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        collect(['Penyusunan Draft', 'Tanda Tangan Rektor', 'Dikirim ke Mitra', 'Diproses Mitra', 'Selesai'])->each(function ($status) {
            StatusKerjasama::create(['nama' => $status]);
        });
    }
}
