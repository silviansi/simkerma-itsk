<?php

namespace Database\Seeders;

use App\Models\Mitra;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class KerjasamaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Gunakan Faker untuk menghasilkan data dummy
        $faker = Faker::create('id_ID');

        // Dapatkan semua ID mitra
        $mitraIds = Mitra::pluck('id')->toArray();

        for ($i = 0; $i < 15; $i++) {
            $mitraId = $faker->randomElement($mitraIds); // Pilih ID mitra secara acak

            DB::table('data_kerjasama')->insert([
                'tanggal_mou' => $faker->date(),
                'tanggal_akhir' => $faker->date('Y-m-d', '+2 years'),
                'deskripsi' => $faker->text(40),
                'no_surat_instansi' => $faker->randomNumber(6),
                'no_surat_mitra' => $faker->randomNumber(6),
                'file' => $faker->file('public/Data-template', 'public/Data-Kerjasama', false),
                'jenis_dokumen' => $faker->randomElement(['MOU', 'LOA']),
                'status' => $faker->randomElement(['Penyusunan Draft']),
                'mitra_id' => $mitraId,
            ]);
        }
    }
}
