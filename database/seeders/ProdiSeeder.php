<?php

namespace Database\Seeders;

use App\Models\Prodi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProdiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        collect(['Kebidanan', 'Farmasi Klinis dan Komunitas', 'Fisioterapi', 'Informatika', 'Keperawatan', 'Keperawatan Anestesiologi'])->each(function ($jenis) {
            Prodi::create(['nama' => $jenis]);
        });
    }
}
